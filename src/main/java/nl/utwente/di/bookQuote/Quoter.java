package nl.utwente.di.bookQuote;


public class Quoter {

    public String getBookPrice(String temp){

        double tmp = Double.parseDouble(temp);
        tmp= (tmp*1.8)+32;
        return String.valueOf(tmp);
    }
}
